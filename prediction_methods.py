# import dill as pickle
import pickle
import config as c
import pandas as pd
import os
import sys
import numpy as np
from datetime import datetime, timedelta
import signature_class
from signature_class import oneSignature, sigDeck  # NEEDED for "pickle.load"
from pull_crypto_data_no_ohlcv import get_pd_attempts
import RC


from sklearn.covariance import EllipticEnvelope
from sklearn.ensemble import IsolationForest
from sklearn.cluster import KMeans, DBSCAN
from sklearn.mixture import GaussianMixture
import util

logger = util.get_logger(__name__)


def pickle_load(folder2path, nameFile):
    with open(os.path.join(folder2path, nameFile), 'rb') as handle:
        return pickle.load(handle)


def load_reservoirs(method='', onlyRecent=False):
    """
    Go through all files saved in c.sigmat_folder and load them to output-dictionary.
    @param method: which reservoirs to load
    @param onlyRecent: want to take just more recent 15 reservoirs?
    @return: dictionary. keys: path to saved file; value: deck (see signature_class.py)
    """
    all_reservoirs = {}
    folder2sigMat = c.sigmat_folder.replace('/', '')
    file_list = os.listdir(folder2sigMat)
    if onlyRecent:  # take 15 most recent, because plotting everything can take long
        file_list_paths = [folder2sigMat+'/'+name for name in file_list]
        file_list = [os.path.split(x)[1] for x in sorted(file_list_paths, key=os.path.getmtime, reverse = True)[:15]]
    if method == 'random':
        file_list = [x for x in file_list if x.endswith('_RAND.pl')]
    elif method == 'benchmark':
        file_list = [x for x in file_list if x.endswith('_BENCH.pl')]
    else:
        file_list = [x for x in file_list if x.endswith('.pl') and '_RAND.pl' not in x and '_BENCH.pl' not in x]
    for file_name in file_list:
        if os.path.getsize(os.getcwd()+c.sigmat_folder+file_name) > 1:
            all_reservoirs[file_name] = pickle_load(folder2sigMat, file_name)
    return all_reservoirs


def select_time_dict(reservoir_dict, days_before_after):
    # Select time of the elements inside the dictionary output of load_reservoirs in a symmetric way wrt the PD date
    assert days_before_after <= 7, 'days_before_after has to be <= 7'
    for key, value in reservoir_dict.items():
        pd_datetime = datetime.strptime(key.split("_btc_",1)[1].replace('.pl',''), "%Y-%m-%d %H.%M")
        reservoir_dict[key].deck = [x for x in reservoir_dict[key].deck if
                                    x.firstDate.to_pydatetime() >= pd_datetime - timedelta(days=days_before_after) and
                                    x.endDate.to_pydatetime() <= pd_datetime + timedelta(days=days_before_after)]
    return reservoir_dict


def fit_model(name, algorithm, x_train, fit_pred, return_scores=True):
    """
    @param name: name of the algorithm to fit
    @param algorithm: sklearn type model. Must have a fit method.
    @param x_train: training data set
    @param fit_pred: use method fit_predict
    @param return_scores: returns scores instead of predictions
    @return: predicted outliers or scores
    """
    fitted_model = None
    if name == "Local Outlier Factor" or fit_pred:
        y_pred = algorithm.fit_predict([_.sig.flatten() for _ in x_train.deck])
    else:
        # fit and predict immediately as UNSUPERVISED LEARNING
        input = [_.sig.flatten() for _ in x_train.deck]
        fitted_model = algorithm.fit(input)
        y_pred = fitted_model.predict(input)
    if return_scores:
        try:
            return algorithm.score_samples([_.sig.flatten() for _ in x_train.deck]), None
        except AttributeError:
            return algorithm.negative_outlier_factor_, None
    else:
        return y_pred, fitted_model


def run_algo(algoList, x_train, fit_pred=False, return_score=False):
    pred = {}
    if c.train_symbols_separate:  # first trial --> should be avoided
        for algoName, algorithm in algoList:
            logger.info(f'Fitting model {algoName}')
            for file_name, val in x_train.items():
                pred[(algoName, file_name)] = fit_model(algoName, algorithm, val, fit_pred, return_score)
    else:
        decks = []
        for sig_deck in x_train.values():
            decks += sig_deck.deck
        all_decks = sigDeck(decks)
        for algoName, algorithm in algoList:
            logger.info(f'Fitting model {algoName}')
            predictions, fitted_model = fit_model(algoName, algorithm, all_decks, fit_pred, return_score)
            if algoName == 'DBSCAN':  # to study DBSCAN as quite complex
                special_output_DBSCAN(fitted_model)
            start = 0
            end = 0
            for file_name, val in x_train.items():
                end += len(val.deck)
                pred[(algoName, file_name)] = predictions[start:end]
                start += len(val.deck)
    return pred


def special_output_DBSCAN(fitted_model):
    from sklearn import metrics
    labels = fitted_model.labels_
    # Number of clusters in label   s, ignoring noise if present.
    n_clusters_ = len(set(labels))-(1 if -1 in labels else 0)
    n_noise_ = list(labels).count(-1)
    print('Estimated number of clusters: %d'%n_clusters_)
    print('Estimated number of noise points: %d'%n_noise_)


@util.print_runtime
def clustering_algorithms(x_train, nClusters=c.cluster_number, nInit=c.startAlgo_number):
    """
    Run function run_algo on the previously listed clustering algorithms
    ref: https://scikit-learn.org/stable/modules/clustering.html
    @param x_train: training data set
    @param nClusters: number of clusters (if required)
    @param nInit: number of restart of the algorithm (if required)
    """
    ## NOTE: SpectralClustering requires huge amount of memory
    clustering_algo = [
        ('KMeans', KMeans(n_clusters=nClusters, init='k-means++', n_init=nInit)),
        # ('SpectralClustering', SpectralClustering(n_clusters=nClusters, n_components=2*nClusters, n_init=nInit,
        #                                           assign_labels='discretize', random_state=0)),
        ('GaussianMixture', GaussianMixture(n_components=nClusters, n_init=nInit//2, random_state=0)),  # VERY SLOW
        ('DBSCAN', DBSCAN(eps=0.3, min_samples=20))  # one needs to check how many clusters finds
        # ('OPTICS', )  # generalization of DBSCAN (quite complex)
    ]
    return run_algo(clustering_algo, x_train, fit_pred=True, return_score=False)


@util.print_runtime
def anomaly_detection_algorithms(x_train, outliers_fraction=0.15):
    """
    ref: https://scikit-learn.org/stable/auto_examples/miscellaneous/plot_anomaly_comparison.html#sphx-glr-auto-examples-miscellaneous-plot-anomaly-comparison-py
    @return: predictions as a dictionary
    """
    anomaly_algo = [
        ("Robust covariance", EllipticEnvelope(contamination=outliers_fraction)),
        # ("One-Class SVM", svm.OneClassSVM(nu=outliers_fraction, kernel="rbf", gamma=0.1)),
        ("Isolation Forest", IsolationForest(contamination=outliers_fraction, random_state=42)),
        # ("Local Outlier Factor", LocalOutlierFactor(n_neighbors=35, contamination=outliers_fraction))
        ]
    return run_algo(anomaly_algo, x_train, return_score=True)


def get_is_pd(symbol, start, end, pd_df):
    """
    @param symbol: symbol of currency
    @param start: start of reservoir window
    @param end: end of reservoir window
    @param pd_df: dataframe containing pump-dump attempts
    @return: Is window given by start-end included inside PD-attempt-time +/- 30 minutes?
    """
    # start = datetime.fromisoformat(start[:-5])
    # end = datetime.fromisoformat(end[:-5])
    for _, pd_attempt in pd_df[pd_df['symbol'] == symbol].iterrows():
        pd_start = datetime.fromisoformat(pd_attempt['date'] + 'T' + pd_attempt['hour'])
        pd_end = pd_start + timedelta(hours=0.5)
        pd_start = pd_start + timedelta(hours=-0.5)
        if (end > pd_start) and (start < pd_end):
            return True
    return False

# NOTE: the use of the function get_is_pd implies that there was an PD attempt iff the window under scrutiny is strictly
# included inside the 1-hour time-window around the official PD time
def eval_preprocess(reservoir_dict, pd_df, predictions):
    """
    Preprocessing the model outputs in order to more easily evaluate the performance
    @param reservoir_dict: reservoirs for computation
    @param pd_df: pump and dump dataframe
    @param predictions: model output
    @return:
    """
    logging_counter = 0
    methods = np.unique([_[0] for _ in predictions.keys()])
    all_rows = []
    for file_name, res in reservoir_dict.items():
        logging_counter += 1
        if logging_counter % 5 == 0:
            logger.debug(f'Computed {logging_counter} symbols')
        for row_num, single_obs in enumerate(res.deck):
            start = single_obs.firstDate
            end = single_obs.endDate
            is_pd = get_is_pd(file_name.split('_')[1], start, end, pd_df)
            for method in methods:
                try:
                    row = [method, file_name, file_name.split('_')[1], is_pd,
                           predictions[(method, file_name)][row_num], start, end]
                    all_rows.append(row)
                except IndexError:
                    pass
    enriched_eval_df = pd.DataFrame(all_rows, columns=['Method', 'Filename', 'Symbol', 'Is_Pump_Dump', 'Prediction',
                                             'Window_start', 'Window_end'])
    return enriched_eval_df


def pickle2csv_name(sig_pickle_name):
    return sig_pickle_name.replace('.pl', '.csv')


def open_associated_csv(pickle_file_name):
    csv_name = pickle2csv_name(pickle_file_name)
    df = pd.read_csv(os.path.join(c.raw_data_folder.replace('/', ''), csv_name))  # LOAD Time Series
    df['datetime'] = [datetime.fromisoformat(str_date.replace("Z", "+00:00")) for str_date in df['datetime']]
    return df, csv_name


@util.print_runtime
def main():
    useRandSig = True
    logger.info('Starting predictions.')
    pd_attempts = get_pd_attempts(remove=True)
    try:
        reservoirs = load_reservoirs()
        # reservoirs_bench = load_reservoirs(method='benchmark')
    except FileNotFoundError:
        logger.error('Reservoirs not created yet.')
        return

    # Anomaly detection algorithms on standard signature
    if not os.path.exists(os.path.join(c.prediction_output, 'predictions.pl')):
        predictions = anomaly_detection_algorithms(x_train=reservoirs)
        with open(os.path.join(c.prediction_output, 'predictions.pl'), 'wb') as handle:
            pickle.dump(predictions, handle)
    else:
        predictions = pickle.load(open(os.path.join(c.prediction_output, 'predictions.pl'), 'rb'))
    eval_df = eval_preprocess(reservoirs, pd_attempts, predictions)
    eval_df.to_csv(os.path.join(c.prediction_output, 'reservoirs_predictions.csv'))

    # Anomaly detection algorithms on random signature
    if useRandSig:
        reservoirs_rand = load_reservoirs(method='random')
        if not os.path.exists(os.path.join(c.prediction_output, 'predictions_rand.pl')):
            predictions_rand = anomaly_detection_algorithms(x_train=reservoirs_rand)
            pickle.dump(predictions_rand, open(os.path.join(c.prediction_output, 'predictions_rand.pl'), 'wb'))
        else:
            predictions_rand = pickle.load(open(os.path.join(c.prediction_output, 'predictions_rand.pl'), 'rb'))
        eval_df = eval_preprocess(reservoirs_rand, pd_attempts, predictions_rand)
        eval_df.to_csv(os.path.join(c.prediction_output, 'random_reservoirs_predictions.csv'))


if __name__ == '__main__':
    main()
