import numpy as np
from dataclasses import dataclass
from typing import List

@dataclass
class oneSignature:
    firstDate: str
    endDate: str
    sig: np.ndarray
    includePD: bool

    def __repr__(self):
        return f'start: {self.firstDate} end: {self.endDate} sig:{self.sig.shape} include PumpDump:{self.includePD}'


@dataclass()
class sigDeck:
    deck: List[oneSignature]

    def __repr__(self):
        return f'Reservoir deck of length {len(self.deck)}'