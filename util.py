import logging
from logging.handlers import TimedRotatingFileHandler
import os
import sys
import time
import inspect
from datetime import timedelta
import numpy as np

os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'
console = logging.StreamHandler(sys.stdout)
FORMATTER = "%(asctime)s — %(name)s — %(levelname)s — %(message)s"
console.setFormatter(logging.Formatter("%(asctime)s — %(name)s — %(levelname)s — %(message)s"))
LOG_FILE = "logs/market_manipulation.log"


def get_logger(logger_name):
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(logging.Formatter(FORMATTER))
    logging.basicConfig(format=FORMATTER,
                        handlers=[TimedRotatingFileHandler(filename=LOG_FILE, when='midnight', backupCount=90),
                                  console_handler])
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)  # better to have too much log than not enough
    return logger


def print_runtime(fct):
    logger = get_logger(__name__)

    def return_fct(*args, **kwargs):
        start = time.perf_counter()
        results = fct(*args, **kwargs)
        logger.info(f'Elapsed time for {inspect.getfile(fct)}.{fct.__name__}: '
                    f'{timedelta(seconds=(round(time.perf_counter() - start, 2)))}')
        logger.debug(f'Arguments: {args}, {kwargs}')
        return results

    return return_fct


def get_precision(results, labels, cutoff):
    estimations = np.where(results >= cutoff)[0]

    tp = 0
    for key in labels:
        if any(np.isin(labels[key], estimations)):
            tp += 1
    duplicate_estimations = 0  # remove consecutive estimates (similar to "pausing the detector in Morgia paper)
    for i in range(len(estimations)):
        if estimations[i] + 1 in estimations:
            duplicate_estimations += 1

    fp = len(estimations) - min(duplicate_estimations, round(len(estimations) / 2)) - tp
    if (tp + fp) == 0:
        return 1
    else:
        return tp / (tp + fp)


def get_recall(results, labels, cutoff):
    estimations = np.where(results >= cutoff)[0]
    tp = 0
    for key in labels:
        if any(np.isin(labels[key], estimations)):
            tp += 1
    fn = len(labels) - tp
    if (tp + fn) == 0:
        return 1
    else:
        return tp / (tp + fn)
