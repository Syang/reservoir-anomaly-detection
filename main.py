import obtainSignatures
import numpy as np
import prediction_methods
import plot_ts
import util


@util.print_runtime
def runall():
    np.random.seed(10)
    obtainSignatures.run()
    prediction_methods.main()
    plot_ts.main_plotAlgorithms()


if __name__ == '__main__':
    runall()
